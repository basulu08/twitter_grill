
(function () {
    'use strict'

    let linkStyle = document.createElement('link');
    linkStyle.rel = 'stylesheet';
    linkStyle.type = 'text/css';
    linkStyle.href = chrome.extension.getURL('css/content.css');
    (document.head||document.documentElement).appendChild(linkStyle);


    setTimeout(() => {
        let items = document.querySelectorAll('.js-stream-item.stream-item.stream-item');
        for (let item of items) {
            let cap_img = document.createElement('img') ;
            cap_img.className = 'cap-santa';
            cap_img.src = chrome.extension.getURL('images/santa.png');
            item.appendChild(cap_img);
        }
    }, 1000);
})();